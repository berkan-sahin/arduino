#define YAK_BTN 7
#define SON_BTN 6
#define LED 10 //ledi cizgi olan girislerden birine baglamaya dikkat edin
bool ledYanik = false; //ilk basta sondurmeye basinca yanmasin diye

void setup() {
    pinMode(YAK_BTN, INPUT_PULLUP);
    pinMode(SON_BTN, INPUT_PULLUP);
    pinMode(LED, OUTPUT);
}

void loop() {
    if(digitalRead(YAK_BTN) == LOW && ledYanik == false)
        for(int i = 0; i <= 255; i++) {
            analogWrite(LED, i);
            delay(4); // (1000 / 255) yaklasik degeri
            ledYanik = true;
        }
    else if(digitalRead(SON_BTN) == LOW && ledYanik == true)
        for(int i = 255; i >= 0; i--) {
            analogWrite(LED, i);
            delay(4); // (1000 / 255) yaklasik degeri
            ledYanik = false;
        }
    
}