#define LED 10 // Sabit deger atama
int sure = 1000; //Degisken tanimlama
void setup(){
    //Buradaki kodlar arduino ilk acildiginda calisir
    pinMode(LED, OUTPUT); //kullandigimiz portun giris mi cikis mi oldugunu belirtme
}

void loop(){
    //Buradaki kodlar yukaridakilerden sonra surekli calisir
    digitalWrite(LED, HIGH); 
    delay(sure); // 1000 ms bekle
    digitalWrite(LED, LOW);
    delay(sure);
}