#define LED 13
char deger;
void setup(){
    Serial.begin(9600);
    pinMode(LED, OUTPUT);
    digitalWrite(LED, OUTPUT);
}

void loop(){
    if(Serial.available()>0){  //Serial porttan veri geliyorsa
        deger = Serial.read(); //Veriyi oku
        if(deger == '1')      //Eğer en son gelen veri 1 ise ledi yak, değilse söndür
            digitalWrite(LED, HIGH);
        else
             digitalWrite(LED, LOW);
         
    }
    delay(200);
}
