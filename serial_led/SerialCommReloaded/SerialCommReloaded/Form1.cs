﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace SerialCommReloaded
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void baglan_Click(object sender, EventArgs e)
        {
            if (!(serialPort1.IsOpen))          // İletişim başlamdıysa 
            {
                serialPort1.PortName = "COM10"; // COM10 yerine kendi portunuzu yazın
                serialPort1.Open();             // İletişimi başlat
                MessageBox.Show("anah");
            }
        }

        private void yak_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)             // İletişim varsa
            {
                serialPort1.Write("1");         // Arduinoya 1 gönder
            }
        }

        private void sondur_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)             // İletişim varsa
            {
                serialPort1.Write("0");         // Arduinoya 0 gönder
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Program Files (x86)\Arduino\arduino.exe");
        }

        // aga sınavda çıkmıcak
        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"iexplore.exe", "https://www.youtube.com/watch?v=7-AKzrDPBwE");
        }
    }
}
