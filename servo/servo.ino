#include <Servo.h> //Servo icin bu kutuphane lazim
#define POTPIN 2 // Potansiyometre baglanan pin
int deger;
Servo muhservo; // muhservo adinda bir servo tanimladik
void setup(){
    muhservo.attach(9); //Servomu 9. pine bagladim
    // Servo baglanan pinlerin yaninda cizgi olan pin olmasina dikkat edilmeli
}

void loop(){
    deger = map(analogRead(POTPIN), 0, 1023, 0, 180); //potansiyometre degerini istenilen aciya cevirdim
    muhservo.write(deger);  //servoyu istedigim pozisyona aldim
    delay(200);
}