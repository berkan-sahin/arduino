#define SOL_LED 10
#define SAG_LED 9
#define POT 5
int deger  = 512;
int sag_p  = 0;
int sol_p  = 0;
void setup() {
  pinMode(SOL_LED, OUTPUT);
  pinMode(SAG_LED, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  deger = analogRead(POT); //0 ile 1024 arasinda deger geliyor
  Serial.print(deger);
  /* Map fonksiyonu
   map(x, in_min, in_max, out_min, out_max)
   x = giris degeri
   in_min = minimum giris degeri
   in_max = maksimum giris degeri
   out_min = in_min icin cikis degeri
   out_max = in_max icin cikis degeri
   
   Bu fonksiyonda giris ve cikis icin max-min oranlanarak gelen veri istenilen degere cekiliyor
   */
  sol_p = map(deger, 500, 0, 0, 255); //Deger azaldikca artiyor
  sag_p = map(deger, 600 ,1023,0,255); //Deger arttikca artiyor
  if(sol_p < 0)
    sol_p = 0;
  if(sag_p < 0)
    sag_p = 0;
  Serial.print(" Sol: ");
  Serial.print(sol_p);
  Serial.print(" Sag: ");
  Serial.println(sag_p);
  
  analogWrite(SOL_LED, sol_p); // 0 ile 255 arasında değer veriliyor
  analogWrite(SAG_LED, sag_p); 

}

