#define LED 10
#define BUTON 7
void setup(){
    pinMode(LED, OUTPUT);
    pinMode(BUTON, INPUT_PULLUP); //Arduino icindeki pullup modunu aktif et
}

void loop(){
    if(digitalRead(BUTON) == LOW) //Pull up modunda buton basiliyken low, degilken high gelir
        digitalWrite(LED, HIGH);
    else
        digitalWrite(LED, LOW);
        
}