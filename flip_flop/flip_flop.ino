#define LED1  8 // Sabit deger tanimlama
#define LED2  9 
#define LED3  10 
int sure = 300; // Degisken deger tanimlama
void setup() {
    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
    pinMode(LED3, OUTPUT);
}


void loop() {
    digitalWrite(LED1, HIGH);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, LOW);
    delay(sure);
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, HIGH);
    delay(sure);
    digitalWrite(LED2, LOW);
    digitalWrite(LED3, HIGH);
    delay(sure);
    digitalWrite(LED3, LOW);
    digitalWrite(LED2, HIGH);
    delay(sure);
}