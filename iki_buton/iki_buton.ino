#define LED 10
#define BUTON1 7
#define BUTON2 6
void setup(){
    pinMode(LED, OUTPUT);
    pinMode(BUTON1, INPUT_PULLUP);
    pinMode(BUTON2, INPUT_PULLUP);
}

void loop(){
    if(digitalRead(BUTON1) == LOW)
        digitalWrite(LED, HIGH);
    if(digitalRead(BUTON2) == LOW)
        digitalWrite(LED, LOW);
        
}