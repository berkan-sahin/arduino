# Arduino eğitim projeleri
Buradaki projeler sürekli olarak güncellenmektedir. Sağ üst köşeden en güncel halinin .zip dosyasını indirebilirsiniz veya tarayıcınızdan devre ve kodlara erişebilirsiniz.

![ ](https://gitlab.com/berkan-sahin/arduino/raw/master/down.png "Bilgisayara indirme")

## Proje sıralaması(basitten karmaşığa)
* blink
* flip_flop
* buton
* iki_buton
* pot
* pot_serial
* yavas_yan_son
* iki_led
* servo
