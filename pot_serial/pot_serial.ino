#define LED 10
#define POT 5
int sure = 1000;
void setup(){
    pinMode(LED, OUTPUT);
    Serial.begin(9600); //9600 kanalinda iletisim baslat
}

void loop(){
    sure = analogRead(POT); //0 ve 1024 arasinda deger geliyor
    Serial.print("Sure: "); //Print ekrana normal sekilde yazdirir
    Serial.print(sure);     //Print icine degisken de yazabilirsiniz
    Serial.println("ms");   //Println ise yazdirdiktan sonra alt satira gecer
    digitalWrite(LED, HIGH);
    delay(sure);
    digitalWrite(LED, LOW);
    delay(sure);
}